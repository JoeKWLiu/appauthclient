import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userInfo: {},
    //domain: `https://appcloud.fpcetg.com.tw/apppemtest/api/`
    domain: `http://10.153.196.79:1397/api/`
  },
  mutations: {
    setUserInfo: function(state, user){ state.userInfo = user;},
  },
  actions: {
  },
  modules: {
  }
})
