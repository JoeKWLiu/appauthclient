//import '@babel/polyfill';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'typeface-roboto/index.css'

import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(VueCookies)

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
