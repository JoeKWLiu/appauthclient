import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    component: Home,
    children: [
      {
        path: '',
        name: 'App',
        component: () => import('../components/AppManage.vue')
      },
      {
        path: '/demo',
        name: 'Demo',
        component: () => import('../components/Demo.vue')
      },
      {
        path: '/group/:appID',
        name: 'Group',
        component: () => import('../components/GroupManage.vue'),
        props: true,
      },
      {
        path: '/function/:appID/:functionGroupID',
        name: 'Function',
        component: () => import('../components/FunctionManage.vue'),
        props: true,
      },
      {
        path: '/subfunction/:appID/:functionGroupID/:functionID',
        name: 'Function',
        component: () => import('../components/SubFunctionManage.vue'),
        props: true,
      },
      {
        path: '/priv/user/:appID/:functionGroupID/:functionID?/:subfunctionID?',
        name: 'PrivUser',
        component: () => import('../components/PrivUserManage.vue'),
        props: true,
      },
      {
        path: '/priv/dept/:appID/:functionGroupID/:functionID?/:subfunctionID?',
        name: 'PrivDept',
        component: () => import('../components/PrivDeptManage.vue'),
        props: true,
      },
      {
        path: '/priv/group/:appID/:functionGroupID/:functionID?/:subfunctionID?',
        name: 'PrivGroup',
        component: () => import('../components/PrivGroupManage.vue'),
        props: true,
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/Login.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
