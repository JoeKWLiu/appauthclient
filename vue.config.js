module.exports = {
  //"transpileDependencies": [
  //  "vuetify"
  //],
  //publicPath: process.env.NODE_ENV === 'production'
  //  ? '/apppemtest/dist/'
  //  : '/'
  publicPath: process.env.NODE_ENV === 'production'
    ? '/dist/'
    : '/',
}